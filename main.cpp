#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <set>
#include <iomanip>
#include "Matrix.h"
using namespace std;
struct element{
    string type;
    int src;
    int dst;
    double value;
    double InitVal;
};
vector <element> Vsrc;
vector <element> Isrc;
vector <element> Res;
vector <element> Ind;
vector <element> Cap;
bool iterative;
int N;
int M;
int L;
double h=0.1;
void read_input(string filename)
{
    set <int> nodes;
    ifstream in;
    in.open(filename);
    if(!in)
    {
        cout<<"error opening file\n";
        exit(0);
    }
    while(!in.eof())
    {
        element temp;
        in>>temp.type>>temp.src>>temp.dst>>temp.value>>temp.InitVal;
        nodes.insert(temp.src);
        nodes.insert(temp.dst);
        if (temp.type=="Vsrc"){
            Vsrc.push_back(temp);
            M++;
        }
        else if(temp.type=="Isrc")
            Isrc.push_back(temp);
        else if(temp.type=="R")
            Res.push_back(temp);
        else if(temp.type=="I"){
            Ind.push_back(temp);
            iterative=true;
            L++;
            element t;
            t.src=temp.src;
            t.dst=temp.dst;
            t.InitVal=0;
            t.type="Ind";
            t.value=0;
            Vsrc.push_back(t);
        }
        else if(temp.type=="C"){
            Cap.push_back(temp);
            iterative=true;
            element t;
            t.src=temp.src;
            t.dst=temp.dst;
            t.InitVal=0;
            t.type="R";
            t.value=h/temp.value;
            Res.push_back(t);
        }
    }
    N=*(nodes.rbegin());
    in.close();

}

void print_matrix(double** m,int rows,int cols)
{
    for(int i=0;i<rows;i++)
    {
        for(int j=0;j<cols;j++)
            cout<<" "<<m[i][j];
        cout<<endl;
    }
    cout<<"************************************\n";
}
void init_G(double** G)
{
    for(int i=0;i<Res.size();i++)
    {
        int node1=Res[i].src-1;
        int node2=Res[i].dst-1;
        if(node1>=0&&node2>=0)
        {
            G[node1][node2]+=-1*(1/Res[i].value);
            G[node2][node1]+=-1*(1/Res[i].value);
        }
        if(node1>=0)
        {
            G[node1][node1]+=(1/Res[i].value);
        }
        if(node2>=0)
        {
            G[node2][node2]+=(1/Res[i].value);
        }

    }
}

void init_B(double** B)
{
    for(int i=0;i<Vsrc.size();i++)
    {
        int node1=Vsrc[i].src-1;
        int node2=Vsrc[i].dst-1;
        if(node1>=0)
        {
            B[node1][N+i]=1;
        }
        if(node2>=0)
        {
            B[node2][N+i]=-1;
        }
    }
}
void init_C(double** C)
{
    for (int i=0;i<Vsrc.size();i++)
        for(int j=0;j<N;j++)
            C[N+i][j]=C[j][N+i];
}
void init_Z(double** Z)
{
    for(int i=0;i<Isrc.size();i++)
    {
        int node1=Isrc[i].src-1;
        int node2=Isrc[i].dst-1;
        if(node1>=0)
            Z[node1][0]+=Isrc[i].value;
        if(node2>=0)
            Z[node2][0]-=Isrc[i].value;
    }
    int i=0;
    int j=0;
    while(j<Vsrc.size())
    {
        if(Vsrc[j].type=="Vsrc"){
            Z[N+i][0]=Vsrc[j].value;
            i++;
        }
        j++;
    }
}
void init_D(double** D)
{
    for (int i=0;i<Ind.size();i++)
    {
        D[N+M+i][N+M+i]=-(Ind[i].value/h);
    }
}

int main()
{

    N=0;
    M=0;
    L=0;
    iterative=false;
    read_input("input2.txt");
    cout<<"N = "<<N<<" M = "<<M<<" L = "<<L<<endl;
    if(iterative==false)
    {

        //intialize matrix A
        double** Matrix_A=new double*[N+M];
        for(int i=0;i<N+M;i++)
            Matrix_A[i]=new double[N+M];
        Matrix m;
        m.initilzeMatrix(Matrix_A,N+M,N+M);
        print_matrix(Matrix_A,N+M,N+M);
        //intialize G matrix
        init_G(Matrix_A);
        print_matrix(Matrix_A,N+M,N+M);
        //intialize B matrix
        init_B(Matrix_A);
        print_matrix(Matrix_A,N+M,N+M);
        //intialize C matrix
        init_C(Matrix_A);
        print_matrix(Matrix_A,N+M,N+M);
        //intialize matrix Z
        double** Matrix_Z=new double*[N+M];
        for(int i=0;i<N+M;i++)
            Matrix_Z[i]=new double[1];
        m.initilzeMatrix(Matrix_Z,N+M,1);
        print_matrix(Matrix_Z,N+M,1);
        init_Z(Matrix_Z);
        print_matrix(Matrix_Z,N+M,1);
        double** Matrix_A_inv=new double*[N+M];
        for(int i=0;i<N+M;i++)
            Matrix_A_inv[i]=new double[N+M];
        m.MatrixInversion(Matrix_A,N+M,Matrix_A_inv);
        print_matrix(Matrix_A_inv,N+M,N+M);
        double** Matrix_X=new double*[N+M];
        for(int i=0;i<N+M;i++)
            Matrix_X[i]=new double[1];
        m.MatrixMultiplication(Matrix_A_inv,N+M,Matrix_Z,Matrix_X);
        print_matrix(Matrix_X,N+M,1);
        ofstream out;
        out.open("output1.txt");
        for(int i=1;i<=N;i++)
        {
            out<<"V"<<to_string(i)<<endl;
            out<<Matrix_X[i-1][0]<<endl<<endl;
        }
        for(int i=1;i<=M;i++)
        {
            out<<"Ivsrc"<<to_string(i)<<endl;
            out<<Matrix_X[N+i-1][0]<<endl<<endl;
        }

    }
    else
    {
        //intialize matrix A
        double** Matrix_A=new double*[N+M+L];
        for(int i=0;i<N+M+L;i++)
            Matrix_A[i]=new double[N+M+L];
        Matrix m;
        m.initilzeMatrix(Matrix_A,N+M+L,N+M+L);
        print_matrix(Matrix_A,N+M+L,N+M+L);
        //intialize G matrix
        init_G(Matrix_A);
        print_matrix(Matrix_A,N+M+L,N+M+L);
        //intialize B matrix
        init_B(Matrix_A);
        print_matrix(Matrix_A,N+M+L,N+M+L);
        //intialize C matrix
        init_C(Matrix_A);
        print_matrix(Matrix_A,N+M+L,N+M+L);
        //intialize D matrix
        init_D(Matrix_A);
        print_matrix(Matrix_A,N+M+L,N+M+L);
        double** Matrix_Z=new double*[N+M+L];
        for(int i=0;i<N+M+L;i++)
            Matrix_Z[i]=new double[1];
        m.initilzeMatrix(Matrix_Z,N+M+L,1);
        print_matrix(Matrix_Z,N+M+L,1);
        init_Z(Matrix_Z);
        for(int i=0;i<Cap.size();i++)
        {
            int node1=Cap[i].src-1;
            int node2=Cap[i].dst-1;
            if(node1>=0)
                Matrix_Z[node1][0]+=Cap[i].InitVal*(Cap[i].value/h);
            if(node2>=0)
                Matrix_Z[node2][0]-=Cap[i].InitVal*(Cap[i].value/h);
        }
        print_matrix(Matrix_Z,N+M+L,1);
        for (int i=0;i<Ind.size();i++)
        {
            Matrix_Z[N+M+i][0]=-1*Ind[i].InitVal*(Ind[i].value/h);
        }
        print_matrix(Matrix_Z,N+M+L,1);
        double** Matrix_A_inv=new double*[N+M+L];
        for(int i=0;i<N+M+L;i++)
            Matrix_A_inv[i]=new double[N+M+L];
        m.MatrixInversion(Matrix_A,N+M+L,Matrix_A_inv);
        print_matrix(Matrix_A_inv,N+M+L,N+M+L);
        double** Matrix_X=new double*[N+M+L];
        for(int i=0;i<N+M+L;i++)
            Matrix_X[i]=new double[1];
        int iterations=21;
        vector<vector<double>> results;
        for(int i=0;i<iterations;i++)
        {
            m.MatrixMultiplication(Matrix_A_inv,N+M+L,Matrix_Z,Matrix_X);
            vector<double> r;
            for(int k=0;k<N+M+L;k++)
                r.push_back(Matrix_X[k][0]);
            results.push_back(r);
            print_matrix(Matrix_X,N+M+L,1);
            m.initilzeMatrix(Matrix_Z,N+M+L,1);
            //print_matrix(Matrix_Z,N+M+L,1);
            init_Z(Matrix_Z);
            //print_matrix(Matrix_Z,N+M+L,1);
            for(int j=0;j<Cap.size();j++)
            {
                int node1=Cap[j].src-1;
                int node2=Cap[j].dst-1;
                if(node1>=0)
                {
                    if(node2>=0)
                        Matrix_Z[node1][0]+=(Matrix_X[node1][0]-Matrix_X[node2][0])*(Cap[j].value/h);
                    else
                        Matrix_Z[node1][0]+=(Matrix_X[node1][0]-0)*(Cap[j].value/h);

                }
                if(node2>=0)
                {
                    if(node1>=0)
                        Matrix_Z[node2][0]-=(Matrix_X[node1][0]-Matrix_X[node2][0])*(Cap[j].value/h);
                    else
                        Matrix_Z[node1][0]+=(0-Matrix_X[node2][0])*(Cap[j].value/h);

                }
            }
            for (int j=0;j<Ind.size();j++)
            {
                Matrix_Z[N+M+j][0]=-1*Matrix_X[N+M+j][0]*(Ind[j].value/h);
            }


        }


        ofstream out;
        out.open("output2.txt");
        for(int i=1;i<=N;i++)
        {
            out<<"V"<<to_string(i)<<endl;
            for(int j=1;j<results.size();j++)
            {
                out<<j*h<<" "<<setprecision(15)<<results[j-1][i-1]<<endl;
            }
            out<<endl;
        }
        for(int i=1;i<=M+L;i++)
        {
            out<<"Isrc"<<to_string(i)<<endl;
            for(int j=1;j<results.size();j++)
            {
                out<<j*h<<" "<<setprecision(15)<<results[j-1][N+i-1]<<endl;
            }
        }




    }

    return 0;
}

